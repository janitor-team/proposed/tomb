tomb (2.9+dfsg1-1) unstable; urgency=medium

  * New upstream release.
    - Adapt d/patches/* to new release.
    - Add btrfs-progs as Suggests dependency.
  * Update d/t/*
    - Rename d/t/test1 to d/t/create-and-use-a-tomb.
    - Extend test to use ext4 and btrfs as the tomb's file system.
  * Introduce d/NEWS.
  * Update d/copyright.

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Sat, 09 Jan 2021 19:43:00 +0100

tomb (2.8.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.
    - Adapt d/patches/* to new release.
    - Remove patches adopted by upstream.
      - fix-typo-calling-pinentry_assuan_getpass.patch
    - Adapt d/tests/test1 to new release.
      - Always redirect stderr to stdout with calling tomb.
  * Refactor d/tests/test1.
  * Update d/copyright.

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Mon, 30 Nov 2020 18:43:54 +0100

tomb (2.8+dfsg1-1) unstable; urgency=medium

  * New upstream release.
    - Fixes CVE-2020-28638: A static string is injected as enryption key when
      pinentry-curses is used and $DISPLAY is non-empty.
      (Closes: #975084)
    - Adapt d/patches/* to new release.
    - Remove patch not required any more.
      - CVE-2020-28638.patch
    - Remove patches adopted by upstream.
      - fix-default-cipher.patch
      - fix-errors-on-open.patch
    - Add new patch to fix mistyped function call.
      - fix-typo-calling-pinentry_assuan_getpass.patch
  * Update d/copyright.
  * Update d/control.
    - Remove needless field Pre-Depends.
    - Bump Standards-Version to 4.5.1.

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Fri, 27 Nov 2020 19:15:59 +0100

tomb (2.7+dfsg2-2) unstable; urgency=medium

  [ Samuel Henrique ]
  * Add d/gbp.conf.

  [ Debian Janitor ]
  * Remove obsolete field Name from d/u/metadata.
  * Update Standards-Version to 4.5.0, no changes needed.

  [ Sven Geuer ]
  * Security upload (Closes: #974719).
    - CVE-2020-28638: A static string is injected as enryption key when
      pinentry-curses is used and $DISPLAY is non-empty.
  * Add myself as uploader.
  * Helper executables moved from /usr/lib to /usr/libexec.
  * Bump debhelper-compat to 13.
  * Update d/copyright.

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Sun, 15 Nov 2020 00:27:31 +0100

tomb (2.7+dfsg2-1) unstable; urgency=medium

  * Team upload.
  [ Samuel Henrique ]
  * Add d/salsa-ci.yml
  [ Sven Geuer ]
  * New upstream release
    - Adapt d/patches/* to new release
    - Remove patches adopted by upstream
      - fix-spelling-errors-in-manpage.patch
      - fix-unrecognized-eof.patch
  * d/control:
    - Bump Standards-Version to 4.4.1
    - Add Rules-Requires-Root field
  * d/copyright
    - Add various authors and licenses missing for upstream sources
    - Add various authors missing for d/*
    - Group files and authors by license
  * Switch to version dfsg2 of the repacked upstream tarball
    - Drop doc/LinuxHDEncSettings.txt from upstream due to uncertain
      licensing
    - Update Files-Excluded in d/copyright

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Thu, 07 Nov 2019 19:54:54 +0100

tomb (2.6+dfsg1-2) unstable; urgency=medium

  * Team upload.
  [ Sven Geuer ]
  * Add patch d/patches/fix-unrecognized-eof.patch so tomb-kdb-pbkdf2
    reliably detects EOF on all architectures when reading in a password
    (Closes: #935197).
  * d/control:
    - Replace Build-Depends debhelper by debhelper-compat
  * d/compat:
    - Remove file in consequence of Build-Depends change

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Wed, 21 Aug 2019 20:15:10 +0200

tomb (2.6+dfsg1-1) unstable; urgency=medium

  * Team upload.
  [ Sven Geuer ]
  * New upstream release
    - Adapt d/patches/* to new release
  * Add further missing dependencies and correct existing ones
  * Make package lintian clean
    - Add debian/tests/* for autopkgtest
    - Fix for lintian info debian-watch-contains-dh_make-template
    - Add patch for three lintian infos spelling-error-in-manpage
  [ SZ Lin (林上智) ]
  * Remove unnecessary files in .pc
  * d/control:
    - Bump Standards-Version to 4.4.0
  * d/source/lintian-overrides
    - Remove unnecessary lintian-override
  * d/upstream/metadata:
    - Tidy content of metadata

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 07 Aug 2019 16:55:52 +0800

tomb (2.5+dfsg1-3) UNRELEASED; urgency=medium

  * Team upload.
  [ Sven Geuer ]
  * Add several missing Recommends and Suggests (Closes: #924042).
    - gettext-base, lsof, dcfldd, qrencode, unoconv, steghide, swish-e
  * d/control:
    - Bump Standards-Version to 4.3.0
    - Bump DH version to 12
  * d/compat:
    - Bump compat to 12
  * d/copyright:
    - Normalize Copyright fields according to DEP-5
    - Update * copyright
    - Update debian/* copyright
  * Add kdf helper binaries to the package (Closes: #924043)
    - d/control:
      - Change Architecture to 'any'
      - Add required Build-Depends and Depends
    - Add d/patches/include-kdf-binaries.patch
    - d/rules:
      - Add overrides for dh_auto_clean/build/install
  * Fix default cipher
    - Add d/patches/fix-default-cipher.patch (Closes: #930782)
    - d/control:
      - Correct cipher mentioned in the description
  * Fix error messages on opening a new tomb (Closes: #931027)
    - Add d/patches/fix-errors-on-open.patch

 -- Sven Geuer <debmaint@g-e-u-e-r.de>  Mon, 24 Jun 2019 22:37:37 +0200

tomb (2.5+dfsg1-2) unstable; urgency=medium

  * Team upload.
  [ Raphaël Hertzog ]
  * d/control:
    - Update team maintainer address to Debian Security Tools

  [ SZ Lin (林上智) ]
  * Add upstream metadata file
  * d/control:
    - Bump Standards-Version to 4.2.1
  * d/rules:
    - Add override_dh_missing target (--fail-missing)

 -- SZ Lin (林上智) <szlin@debian.org>  Wed, 26 Sep 2018 22:48:40 +0800

tomb (2.5+dfsg1-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * Add e2fsprogs dependency (Closes: #887295)

  [ ChangZhuo Chen (陳昌倬) ]
  * New upstream release.
  * Bump Standards-Version to 4.1.3.
  * Bump compat to 11.
  * Update Vcs-* fields to salsa.debian.org.

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Wed, 14 Feb 2018 13:29:49 +0800

tomb (2.4+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #611660).

 -- ChangZhuo Chen (陳昌倬) <czchen@debian.org>  Fri, 21 Jul 2017 19:09:45 +0800
